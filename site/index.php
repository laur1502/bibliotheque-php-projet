<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Bibliothèque</title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>


<!-- Lier à la base de données -->
<?php
$bdd = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', '');
?>



<body>
    
<header>
    <header id="header1">
        <br>
        <h1>Notre Bibliothèque</h1>
        <div id="livre">
        <img src="Img/livre.png" style="width:10%"> </div>

        <a href="tousleslivres.html"><div class="button button5">Tous les livres</div></a>
        <a href="annee.html"><div class="button button5">Année</div></a>
        <a href="auteur.html"><div class="button button5">Auteur</div></a>
        <a href="genre.html"><div class="button button5">Genre</div></a>
</header>



    <main id="main">
        <div class="wrapper">

        <div class="zero"></div>    

        <!-- Hunger Games Start-->
        <div class="one"> 
        <div id="hungergames">
            <img src="Img/000001.jpg" style="width:55%"></div>
            <hr class="hr1">
        </div>


        <div class="two"> 
        <p class="description">Hunger games </p>
        <p class="description">Suzanne Collins</p>
        <p class="description">Pocket Jeunesse</p>
        <p class="description">Romans science-fiction pour adolescents</p>
        </div>

        <!-- Hunger Games End-->

        <!-- Harry Potter Start-->
        <div class="three">
        <div id="hungergames">
        <img src="Img/000003.jpg" style="width:60%"></div>
        <hr class="hr1">
        </div>

        <div class="four">
            <p class="description">Harry Potter</p>
            <p class="description">Suzanne Collins</p>
            <p class="description">Pocket Jeunesse</p>
            <p class="description">Romans science-fiction pour adolescents</p>
        </div>

        <!-- Harry Potter End-->
        

        <!-- Jojo's Bizarre Adventure Start-->
        <div class="five">
            <div id="hungergames">
            <img src="Img/000002.jpg" style="width:60%"></div>
            <hr class="hr1">
            </div>

        
    
            <div class="six">
                <p class="description">Jojo's Bizarre Adventure</p>
                <p class="description">Suzanne Collins</p>
                <p class="description">Pocket Jeunesse</p>
                <p class="description">Romans science-fiction pour adolescents</p>
            </div>
        <!-- Jojo's Bizarre Adventure End-->


        <!-- got Start-->
            <div class="seven">
                <div id="hungergames">
                <img src="Img/000006.png" style="width:92%"></div>
                <hr class="hr1">
        
                </div>
        
                <div class="eight">
                    <p class="description">Game of Thrones</p>
                    <p class="description">George R.R Martin</p>
                    <p class="description">Pocket Jeunesse</p>
                    <p class="description">Romans science-fiction pour adolescents</p>
                </div>
        <!-- got End-->


        <!-- Siddharta Start-->
        <div class="nine">
            <div id="hungergames">
            <img src="Img/000008.jpg" style="width:60%"></div>
            <hr class="hr1">
            </div>

        
    
            <div class="ten">
                <p class="description">Siddharta</p>
                <p class="description">Suzanne Collins</p>
                <p class="description">Pocket Jeunesse</p>
                <p class="description">Romans science-fiction pour adolescents</p>
            </div>
        <!-- Siddharta End-->

        <!-- Pet Sematary Start-->
        <div class="eleven">
            <div id="hungergames">
            <img src="Img/000004.jpg" style="width:60%"></div>
            <hr class="hr1">
            </div>

        
    
            <div class="twelve">
                <p class="description">Pet Sematary</p>
                <p class="description">Stephen King</p>
                <p class="description">Pocket Jeunesse</p>
                <p class="description">Romans science-fiction pour adolescents</p>
            </div>
        <!-- Pet Sematary End-->

        <!--  sa mère Start-->
        <div class="thirteen">
            <div id="hungergames">
            <img src="Img/000005.jpg" style="width:60%"></div>
            <hr class="hr1">
            </div>

            <div class="fourteen">
                <p class="description">La vie secrète des écrivains</p>
                <p class="description">Guillaume Musso</p>
                <p class="description">Pocket Jeunesse</p>
                <p class="description">Romans science-fiction pour adolescents</p>
            </div>
        <!-- sa mère  End-->        


        <!-- seigneur des anneaux Start-->
        <div class="fifteen">
            <div id="hungergames">
            <img src="Img/000007.jpg" style="width:62%"></div>
            <hr class="hr1">
            </div>

            <div class="sixteen">
                <p class="description">Le seigneur des anneaux</p>
                <p class="description">J.R.R Tolkien</p>
                <p class="description">Pocket Jeunesse</p>
                <p class="description">Romans science-fiction pour adolescents</p>
            </div>
        <!-- seigneur des anneaux  End-->     
        
        <!-- Avenger Start-->
        <div class="seventeen">
            <div id="hungergames">
            <img src="Img/000009.jpg" style="width:60%"></div>
            <hr class="hr1">
            </div>

            <div class="eighteen">
                <p class="description">Avengers Marvel Now T01</p>
                <p class="description">Jerome Opeña</p>
                <p class="description">Pocket Jeunesse</p>
                <p class="description">Romans science-fiction pour adolescents</p>
            </div>
        <!-- Avengers  End-->      

    </div>
</main>
</body>